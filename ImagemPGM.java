/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exercicioprograma2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;


public class ImagemPGM extends Imagem{
    
    private int posicao;
    private char bit;
    private char byteArquivo;
    private String mensagem;
    private char caracter;
    
    ImagemPGM(String caminhoArquivo){
        this.caminhoArquivo = caminhoArquivo;
    }

    public int getPosicao() {
        return posicao;
    }

    public void setPosicao(int posicao) {
        this.posicao = posicao;
    }
    
    /**
     *
     * @param caminhoArquivo
     * @return
     */
    public int inicioMensagem(String caminhoArquivo) throws IOException{
        File arq = new File(caminhoArquivo);
        RandomAccessFile arqImagem = null;
        arqImagem = new RandomAccessFile(arq, "r");
        char carater;
        boolean condicao = true;
        char espaco;
        String newMensagem = "";
            while(condicao){
                caracter = (char) arqImagem.read();
                if(caracter == '#'){
                    arqImagem.read();
                        while(condicao){
                            caracter = (char) arqImagem.read();
                            espaco = ' ';
                            if(caracter != espaco){
                                newMensagem += caracter;
                            } else {
                                break;}
                        }
                }
                
            }
            arqImagem.close();
                return Integer.parseInt(newMensagem);
    }
    
    public String manipulaImagem(String caminhoArquivo) throws FileNotFoundException, IOException{
        File arq = new File(caminhoArquivo);
        RandomAccessFile arqImagem = null;
        arqImagem = new RandomAccessFile(caminhoArquivo, "r");
        arqImagem.seek(caminharParaInicio(caminhoArquivo) + inicioMensagem(caminhoArquivo));
        char newCaracter = 0x00;
        boolean condicao = true;
        String newMensagem;
        int i, b;
            for(i=0;i<=8;i++){
                b = arqImagem.read() & 0x01;
                newCaracter = (char) ((newCaracter << 1) | b);
            } newMensagem = Character.toString(newCaracter);
        while(condicao){
            newCaracter = 0x00;
                for(i =1; i <=8; i++){
                    b = arqImagem.read() & 0x01;
                    newCaracter = (char) ((newCaracter << 1) | b);
                }
                if(newCaracter == '#'){
                    break;
                }
                newMensagem = newMensagem + Character.toString(newCaracter);
                
        }
        
        
            
        return newMensagem;
    }

    @Override
    void aplicarfiltro() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    

}
        

