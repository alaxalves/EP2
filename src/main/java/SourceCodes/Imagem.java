
package SourceCodes;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.*;



abstract class Imagem {
    protected int altura;
    protected int largura;
    protected int numMaxPixels;
    protected String tipo;
    protected BufferedImage imgBuffer;
    protected char[] pixels;
    protected byte[] modPixels;
    protected char[] newImagem;
    protected String numeroMagico;
    protected String caminhoArquivo;
    
    public void obterNumeroMagico(String caminhoArquivo) throws FileNotFoundException, IOException{
        File arq = new File(caminhoArquivo);
        RandomAccessFile arqImagem = null;
        arqImagem = new RandomAccessFile(caminhoArquivo, "r");
        char caracter;
        String numMagico;
       /*modified*/ caracter = (char) arqImagem.read();
        numMagico = Character.toString((char)caracter);
        /*modified*/ caracter = (char) arqImagem.read();
        numMagico = numMagico + Character.toString((char)caracter);
        this.numeroMagico = numMagico;
    }
    public static String lerLinha(FileInputStream arq){
        String linha = "";
        byte leitor;
        try{
            while((leitor = (byte) arq.read()) != '\n'){
                linha = linha + (char) leitor;
            }
        } catch(IOException e){
            e.printStackTrace();
        }
        return linha;
    }
    public BufferedImage abrirImagem() {
	BufferedImage img = new BufferedImage(getLargura(), getAltura(), BufferedImage.TYPE_BYTE_GRAY);
	char[] imgPix = getPixels();
	byte[] pixels;
        int counter =0;
	pixels = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
            while ( counter < getAltura() * getLargura()) {
		pixels[counter] = (byte) imgPix[counter];
		counter++;
            }
	return img;
	}

	public BufferedImage abrirImagemFil(char[] filteredImage) {

	BufferedImage img = new BufferedImage(getLargura(), getAltura(), BufferedImage.TYPE_BYTE_GRAY);
	int counter = 0;
	byte[] pixels;
	pixels = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
            while (counter < getAltura() * getLargura()) {
		pixels[counter] = (byte) filteredImage[counter];
		counter++;
            }
	return img;
	}
        
    public void PegarAltura(String caminhoArquivo) throws IOException{
        
            File arq = new File(caminhoArquivo);
            RandomAccessFile arqImagem = null;
            arqImagem = new RandomAccessFile(arq, "r");
            String newAltura = null;
            char caracter;
            int numLinhas = 0;
            boolean condicao = true;
                while(condicao){
                    caracter = (char) arqImagem.read();
                    if(caracter == '\n'){
                        numLinhas++;  
                        if(numLinhas ==2){
                             break;
                        }
                    } 
            }
            //caracter = (char) arqImagem.read();
            //newAltura = Character.toString(caracter);
            
                while(condicao){
                    caracter = (char) arqImagem.read();
                    if(caracter == ' '){
                        break;
                    } 
                    else if (caracter != ' '){
                        newAltura = newAltura + caracter;
                    }                
                }
            arqImagem.close();
            this.altura = Integer.parseInt(newAltura);  
        }
        
    
    public void PegarLargura(String caminhoArquivo) throws FileNotFoundException, IOException{
        
            File arq = new File(caminhoArquivo);
            RandomAccessFile arqImagem = null;
            arqImagem = new RandomAccessFile(arq, "r");
            String newAltura = null;
            char caracter;
            int numLinhas = 0;
            boolean condicao = true;
                while(condicao){
                    caracter = (char) arqImagem.read();
                    if(caracter == '\n'){
                        numLinhas++;  
                        if(numLinhas ==2){
                             break;
                        }
                    } 
            }
            caracter = (char) arqImagem.read();
            newAltura = Character.toString(caracter);
            
                while(condicao){
                    caracter = (char) arqImagem.read();
                    if(caracter == ' '){
                        break;
                    } 
                    else if (caracter != ' '){
                        newAltura = newAltura + caracter;
                    }                
                }
            arqImagem.close();
            this.altura = Integer.parseInt(newAltura);  
            
    }
    

    public static long caminharParaInicio(String caminhoArquivo) throws FileNotFoundException, IOException{
        File arq = new File(caminhoArquivo);
        RandomAccessFile arqImagem = null;
        arqImagem = new RandomAccessFile(arq, "r");
        int numLinhas = 0;
        boolean condicao = true;
            while(condicao){
                if(arqImagem.read() == '\n'){
                    numLinhas++;
                    if(numLinhas == 4){
                        break;
                    }
                }                
            }
        long posicao = arqImagem.getFilePointer();
        arqImagem.close();
        return posicao;        
    }
    public void setPixels(String caminhoArquivo) throws IOException{
        File arq = new File(caminhoArquivo);
        RandomAccessFile arqImagem = null;
        arqImagem = new RandomAccessFile(caminhoArquivo, "r");
        arqImagem.seek(caminharParaInicio(caminhoArquivo));
        char[] pixels = new char[getAltura() * getLargura()];
        int i;
            for(i =0; i < pixels.length;i++){
                pixels[i] = (char) arqImagem.read();
            }   
        arqImagem.close();
        this.pixels = pixels;
    }

    public char[] getPixels() {
        return pixels;
    }
    
    public int getAltura() {
        return altura;
    }
    
    public int getLargura() {
        return largura;
    }

    public int getNumMaxPixels() {
        return numMaxPixels;
    }

    public String getTipo() {
        return tipo;
    }

    public String getNumeroMagico() {
        return numeroMagico;
    }

    public String getCaminhoArquivo() {
        return caminhoArquivo;
    }

    public void setCaminhoArquivo(String caminhoArquivo) {
        this.caminhoArquivo = caminhoArquivo;
    }
    
    abstract String manipulaImagem(String caminhoArquivo) throws FileNotFoundException, IOException;
    
}
    

