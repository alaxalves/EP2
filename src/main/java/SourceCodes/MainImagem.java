/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SourceCodes;

import java.io.IOException;

/**
 *
 * @author alaxalves
 */
public class MainImagem {
    
    public static void main(String[] args) throws IOException{
        
                String caminhoArquivo = "/home/alaxalves/NetBeansProjects/ExercicioPrograma2.0/img/imagem1.pgm";
		ImagemPGM test = new ImagemPGM(caminhoArquivo);
		Filtros filtrado = new Filtros();
		filtrado.filtroNegativo(caminhoArquivo);
		test.setPixels(caminhoArquivo);
		filtrado.filtroSharpen(caminhoArquivo);
		
		System.out.println(test.manipulaImagem(caminhoArquivo));
                
    }
}
