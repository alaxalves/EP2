/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SourceCodes;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.*;
import java.util.Scanner;


public class ImagemPGM extends Imagem{
    
    private int posicao;
    private char bit;
    private char byteArquivo;
    private String mensagem;
    private char caracter;
    
    ImagemPGM(String caminhoArquivo){
        this.caminhoArquivo = caminhoArquivo;
    }

    public int getPosicao() {
        return posicao;
    }

    public void setPosicao(int posicao) {
        this.posicao = posicao;
    }
    
    

    public /*static*/ int inicioMensagem(String caminhoArquivo) throws IOException{
        
        File arq = new File(caminhoArquivo);
        RandomAccessFile arqImagem = null;
        arqImagem = new RandomAccessFile(arq, "r");
        char carater;
        boolean condicao = true;
        char espaco = ' ';
        String newMensagem = "";
            while(condicao){
                caracter = (char) arqImagem.read();
                if(caracter == '#'){
                    arqImagem.read();
                        while(condicao){
                            caracter = (char) arqImagem.read();
                            espaco = ' ';
                            if(caracter != espaco){
                                newMensagem += caracter;
                            } else {
                                break;}
                        } break;
                }
            }
            arqImagem.close();
            return Integer.parseInt(newMensagem);
    }
    
    public String manipulaImagem(String caminhoArquivo) throws FileNotFoundException, IOException{
        File arq = new File(caminhoArquivo);
        RandomAccessFile arqImagem = null;
        arqImagem = new RandomAccessFile(caminhoArquivo, "r");
        arqImagem.seek(caminharParaInicio(caminhoArquivo) + inicioMensagem(caminhoArquivo));
        char newCaracter = 0x00;
        boolean condicao = true;
        String newMensagem;
        int i, b;
            for(i=0;i<=8;i++){
                b = arqImagem.read() & 0x01;
                newCaracter = (char) ((newCaracter << 1) | b);
            } newMensagem = Character.toString(newCaracter);
        while(condicao){
            newCaracter = 0x00;
                for(i =1; i <=8; i++){
                    b = arqImagem.read() & 0x01;
                    newCaracter = (char) ((newCaracter << 1) | b);
                }
                if(newCaracter == '#'){
                    break;
                }
                newMensagem = newMensagem + Character.toString(newCaracter);
                
        }
        
    return newMensagem;
    }
    
    public BufferedImage leituraImagem() throws IOException{
        FileInputStream arq = new FileInputStream(getCaminhoArquivo());
        byte byte1;
	String linha;
        linha = ImagemPGM.lerLinha(arq);
	if ("P5".equals(linha)) {
            linha = ImagemPGM.lerLinha(arq);
        	while (linha.startsWith("#")) {
                	linha = ImagemPGM.lerLinha(arq);
		}
        Scanner s = new Scanner(linha);
	if (s.hasNext() && s.hasNextInt()) {
            largura = s.nextInt();
        }
	if (s.hasNext() && s.hasNextInt()) {
            altura = s.nextInt();
	}
	linha = ImagemPGM.lerLinha(arq);
	numMaxPixels = s.nextInt();
	s.close();
        imgBuffer = new BufferedImage(largura, altura, BufferedImage.TYPE_BYTE_GRAY);
	modPixels = ((DataBufferByte) imgBuffer.getRaster().getDataBuffer()).getData();
        int contador = 0;
            while (contador < (largura * altura)) {
		byte1 = (byte) arq.read();
		modPixels[contador] = byte1;
		contador++;
            }
                      
    }
    return imgBuffer; 
}

  

   
    
    

}
        

