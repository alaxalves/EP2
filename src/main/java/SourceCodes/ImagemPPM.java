/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SourceCodes;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.*;
import java.util.Scanner;

/**
 *
 * @author alaxalves
 */
public class ImagemPPM extends Imagem{
    private byte[] pixels;
    private byte[] pixelsInvertidos;

    public ImagemPPM(File arquivo) throws FileNotFoundException, IOException {
        
        FileInputStream arq = null;
        String newCaminhoArquivo = "/home/alaxalves/Imagens/imagem1.pgm";
        arq = new FileInputStream(newCaminhoArquivo);
        Scanner varrer = new Scanner(arq);
        varrer.nextLine();
        varrer.nextLine();
        int newLargura = varrer.nextInt();
        int newAltura = varrer.nextInt();
        int newNumMaxPixels = varrer.nextInt();
        arq.close();
        
        arq = new FileInputStream(newCaminhoArquivo);
        DataInputStream dadoBin = new DataInputStream(arq);
        char skip = '\n';
        int linhas = 4;
            while(linhas>0){
                char car;
                    do{
                        car = (char) (dadoBin.readUnsignedByte());
                    } while(car != skip);
            linhas--;
            }
        int[][] infoImg = new int[newAltura][newLargura];
        int linha, coluna;
        
            for(linha = 0; linha < newAltura; linha++){
                for(coluna = 0; coluna < newLargura; coluna++){
                    infoImg[linha][coluna] = dadoBin.readUnsignedByte();
                    System.out.println(infoImg[linha][coluna]+ " ");
                }   System.out.println();
            
            }        
        
    }
    
    public BufferedImage lerImagem() {
        try {
            FileInputStream arq = new FileInputStream("img/ppm/segredo.ppm");
		int contador = 0;
		String linhas;
		linhas = ImagemPPM.lerLinha(arq);
		if ("P6".equals(linhas)) {
                    linhas = ImagemPPM.lerLinha(arq);
			while (linhas.startsWith("#")) {
				linhas = ImagemPPM.lerLinha(arq);
			}
			Scanner s = new Scanner(linhas);
                            if (s.hasNext() && s.hasNextInt()) {
				largura = s.nextInt();
                            }
                            if (s.hasNext() && s.hasNextInt()) {
				altura = s.nextInt();
                            }
  			linhas = ImagemPPM.lerLinha(arq);
			s = new Scanner(linhas);
			numMaxPixels = s.nextInt();
			s.close();
                        imgBuffer = new BufferedImage(largura, altura, BufferedImage.TYPE_3BYTE_BGR);
			int i, j=2;
			int a = j;
			pixels = new byte[largura * altura * 3];
                            for (i = 0; i < pixels.length; i++) {
				pixels[i] = (byte) arq.read();
                            }	pixelsInvertidos = new byte[pixels.length];
				byte[] pix = new byte[pixels.length];

			for (i = 0; i < pixels.length; i++) {
                		pixelsInvertidos[j] = (byte) pixels[i];
				j--;
				if ((i + 1) % 3 == 0) {
					j = a + 3;
					a += 3;
				}
			}
			pixels = ((DataBufferByte) imgBuffer.getRaster().getDataBuffer()).getData();

			while (contador < 3 * altura * largura) {
                            pixels[contador] = (byte) pixelsInvertidos[contador];
                            contador++;
			}
	}
    } catch (Throwable e) {
	e.printStackTrace();
  }
    return imgBuffer;
}


    @Override
    String manipulaImagem(String caminhoArquivo) throws FileNotFoundException, IOException {
        return null;
    }



  
    
    
    
}
