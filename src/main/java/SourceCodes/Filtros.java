/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SourceCodes;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 *
 * @author alaxalves
 */
public class Filtros extends Imagem{
    
    public BufferedImage filtroBlur(String caminhoArquivo) throws FileNotFoundException, IOException{
       
                BufferedImage imgBlur = new BufferedImage(largura, altura, BufferedImage.TYPE_BYTE_GRAY);
		float[] filtroBlur = new float[9];
                    for (int i = 0; i < 9; i++){
			filtroBlur[i] = 1.0f / 9.0f;
                    }
		BufferedImageOp newImgBlur = new ConvolveOp(new Kernel(3, 3, filtroBlur), ConvolveOp.EDGE_ZERO_FILL, null);
		@SuppressWarnings("unused")
		Object blurredImagem = newImgBlur.filter(imgBuffer, imgBlur);

		return imgBlur;
    }
        
    public void filtroNegativo(String caminhoArquivo) throws FileNotFoundException, IOException{
        File arq = new File(caminhoArquivo);
        RandomAccessFile arqImagem = new RandomAccessFile(arq, "r");
        File newArq = new File("imagemNegative.pgm");
        RandomAccessFile newArqImagem = new RandomAccessFile(newArq, "rw");
        byte[] imgByte = new byte[(int)arqImagem.length()];
        int inicioArq = (int) caminharParaInicio(caminhoArquivo);
        long tamanhoArq = arqImagem.length();
            for(int contador = 0; contador< inicioArq; contador++){
                imgByte[contador] = arqImagem.readByte();
            }
            for(int contador = (inicioArq + 1); contador < tamanhoArq; contador++){
                 imgByte[contador] = (byte) (getNumMaxPixels() - arqImagem.readByte());
            }
        arqImagem.close();
        newArqImagem.write(imgByte);
        newArqImagem.close();
    }
    
  
       
    
    public BufferedImage filtroSharpen(String caminhoArquivo) throws FileNotFoundException, IOException{
       
                BufferedImage imgSharpen = new BufferedImage(largura, altura, BufferedImage.TYPE_BYTE_GRAY);
		float[] filtroSharpen = { 0, -1, 0, -1, 5, -1, 0, -1, 0 };
		BufferedImageOp newImgSharpen = new ConvolveOp(new Kernel(3, 3, filtroSharpen), ConvolveOp.EDGE_ZERO_FILL, null);
		@SuppressWarnings("unused")
		Object sharpenedImagem = newImgSharpen.filter(imgBuffer, imgSharpen);

		return imgSharpen;
    }
     
      
    @Override
    String manipulaImagem(String caminhoArquivo) throws FileNotFoundException, IOException {
        
        return caminhoArquivo;
    }


  
  
}    

