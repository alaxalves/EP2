/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graphics;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class FramePrincipal extends JFrame{
  
    public FramePrincipal(){
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(500, 350);
        JMenuBar menugeral = new JMenuBar();
        setJMenuBar(menugeral);
        JMenu menumsg, menufiltro;
        
        menumsg = new JMenu("Decifrar Mensagem...");
        menugeral.add(menumsg);
        JMenuItem decifraMsg = new JMenuItem("Desvendar mensagem da Imagem.");
        menugeral.add(decifraMsg);
        decifraMsg.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    DecifraMensagem decifra = new DecifraMensagem();
                }        
        });
        
        menufiltro = new JMenu("Aplicar Filtros...");
        menugeral.add(menufiltro);
        JMenuItem aplicaPGM = new JMenuItem("Aplicar na Imagem PGM");
        JMenuItem aplicaPPM = new JMenuItem("Aplicar na Imagem PPM");
        menugeral.add(aplicaPGM);
        menugeral.add(aplicaPPM);
        aplicaPPM.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
               AplicaFiltro aplicappm = new AplicaFiltro();
            }
            
        
        });
    
    }
    
    public static void main(String[] args){
        EventQueue.invokeLater(new Runnable(){
            @Override
            public void run(){
                FramePrincipal framePrincipal = new FramePrincipal();
                boolean condicao = true;
                framePrincipal.setVisible(condicao);
                framePrincipal.setTitle("Exercicio Programa 2.0");
                framePrincipal.getContentPane().setBackground(Color.GRAY);
                            
            }
    
    });
        
  
    }
}
