/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graphics;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.io.*;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author alaxalves
 */
public class DecifraMensagem extends JFrame{
    
    private JPanel painelmsg;
    private JTextField textomsg;
    private SourceCodes.ImagemPGM imagemPGM;
    
    public DecifraMensagem(){
        setTitle("Mensagem Oculta");
        setBounds(100, 100, 450, 350);
        painelmsg = new JPanel();
        painelmsg.setBorder(new EmptyBorder(10, 10, 10, 10));
        setContentPane(painelmsg);
        painelmsg.setLayout(null);
        JButton botaoSelecionarImagem = new JButton("Selecionar imagem...");
        botaoSelecionarImagem.setBounds(100, 25, 30, 12);
        botaoSelecionarImagem.setVerticalAlignment(SwingConstants.NORTH_WEST);
        botaoSelecionarImagem.setHorizontalAlignment(SwingConstants.LEFT);
        botaoSelecionarImagem.setBackground(Color.BLACK);
        painelmsg.add(botaoSelecionarImagem);
        textomsg = new JTextField();
        textomsg.setBounds(50, 80, 200, 25);
        painelmsg.add(textomsg);
        textomsg.setColumns(8);
        boolean condicao = true;
        setVisible(condicao);
        botaoSelecionarImagem.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
               JFileChooser chooser = new JFileChooser();
               chooser.setFileFilter(new FileNameExtensionFilter("PGM & PPM", "pgm", "ppm"));
               chooser.setAcceptAllFileFilterUsed(false);
               chooser.setDialogTitle("Escolha uma Imagem...");
               int val = chooser.showOpenDialog(getParent());
                    if(val == JFileChooser.APPROVE_OPTION){
                        System.out.println("Arquivo escolhido:" + chooser.getSelectedFile().getName());
                        System.out.println("Diretório do arquivo:" + chooser.getSelectedFile().getPath() + "/" + chooser.getDescription(chooser.getSelectedFile()));
                        File arq = chooser.getSelectedFile();
                    }
            }   
        
        });
        
        
        
        
            
    }
    
    
    
}
