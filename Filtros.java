/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exercicioprograma2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 *
 * @author alaxalves
 */
public class Filtros extends Imagem{
    
    public void filtroBlur(String caminhoArquivo) throws FileNotFoundException, IOException{
        File arq = new File(caminhoArquivo);
        RandomAccessFile arqImagem = new RandomAccessFile(arq, "r");
        File newArq = new File("newImagem.pgm");
        RandomAccessFile newArqImagem = new RandomAccessFile(newArq, "rw");
        byte[][] imgByte = new byte[getAltura()][getLargura()];
        int inicioArq = (int) caminharParaInicio(caminhoArquivo);
        long tamanhoArq = arqImagem.length();
        arqImagem.close();
        newArqImagem.close();
        
    }
        
    public void filtroNegativo(String caminhoArquivo) throws FileNotFoundException, IOException{
        File arq = new File(caminhoArquivo);
        RandomAccessFile arqImagem = new RandomAccessFile(arq, "r");
        File newArq = new File("imagemNegative.pgm");
        RandomAccessFile newArqImagem = new RandomAccessFile(newArq, "rw");
        byte[] imgByte = new byte[(int)arqImagem.length()];
        int inicioArq = (int) caminharParaInicio(caminhoArquivo);
        long tamanhoArq = arqImagem.length();
            for(int contador = 0; contador< inicioArq; contador++){
                imgByte[contador] = arqImagem.readByte();
            }
            for(int contador = (inicioArq + 1); contador < tamanhoArq; contador++){
                 imgByte[contador] = (byte) (getNumMaxPixels() - arqImagem.readByte());
            }
        arqImagem.close();
        newArqImagem.write(imgByte);
        newArqImagem.close();
    }
    
    public byte[] convertToBytes(char[] charVec){
        byte[] conversor = new byte[charVec.length];
        int count;
            for(count = 0; count < conversor.length; count++){
                conversor[count] = (byte) charVec[count];
            }
    return conversor;
    }
       
    
    public void filtroSharpen(String caminhoArquivo) throws FileNotFoundException, IOException{
        File arq = new File(caminhoArquivo);
        RandomAccessFile arqImagem = new RandomAccessFile(arq, "r");
        File newArq = new File("imagemSharpen.pgm");
        RandomAccessFile newArqImagem = new RandomAccessFile(newArq, "rw");
        ImagemPGM imgpgm = new ImagemPGM(caminhoArquivo);
        char[] pix = imgpgm.getPixels();
        int tam = 3, den = 1, lim = 3/2;
        int sharpen []= {0, -1, 0, -1, 5, -1, 0, 1, 0};
            for(int linha = lim; linha < getLargura() - lim; linha++){
                for(int coluna = lim; coluna < getAltura() - lim; coluna++){
                    int val = 0;
                    for(int a = -1; a<=1; a++){
                        for(int b = -1; b<=1; b++){
                            val = val + sharpen[(a+1) + tam * (b+1)] * pix[(linha + a)+(coluna+b) * getAltura()];
                        }
                                        
                    }
                    
                    val = val/den;
                    val = val < 0 ? 0 : val ;
                    val = val > 255 ? 255 : val;
                    pix[linha + coluna * getAltura()] = (char) val;
                }
            
            }
            arqImagem.close();
            newArqImagem.write(convertToBytes(pix));
            newArqImagem.close();
    }

    @Override
    String manipulaImagem(String caminhoArquivo) throws FileNotFoundException, IOException {
        
        return caminhoArquivo;
    }

    @Override
    void aplicarfiltro() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  
}    

